'use strict'

import { Usuario } from '../models'

export default {

  findById: (_id) => {
    return Usuario.findById(_id)
  },

  create: (q) => {
    return Usuario.findOne({email:q.email})
    .then(function(usuario){
      if(usuario)
        return {message:'El usuario ya existe'}
      else{
        const usuario = new Usuario(q)
        return usuario.save()
      }
    })
  },

  buscarEmail: async (q) => {
    const data = await Usuario.findOne({email: q.emailReceptor})
    if (data) {
      return data
    } else {
      return null
    }
  },


  loginUsuario: (q) => {
    return Usuario.findOne({ email: q.email})
    .then(function(usuario){
      let status;
      if (!usuario){
        status = 'user'
        return status
      }
      else if(usuario.password!=q.password){
        status = 'pass' 
        return status
      }
      
      return usuario
    })
  },

  consulta: async (q) => {
    const data = await Usuario.findOne({$and:[{documento: q.documento}, {celular: q.celular}]})
    if (data) {
      return data.saldo.toString()
    } else {
      return null
    }
  },

  validar: async (q) => {
    const data = await Usuario.findOne({email: q.email})
    if (data) {
      return data
    } else {
      return null
    }
  },

  update: async (q) => {
    const data = await Usuario.findOne({$and:[{documento: q.documento}, {celular: q.celular}]})
    q.saldo = parseFloat(data.saldo) + parseFloat(q.saldo)
    if (data) {
      return Usuario.updateOne({ _id: data._id }, { $set:  q  })
    } else {
      return null
    }
  },

  update2: (q) => {
    return Usuario.updateOne({ _id: q._id }, { $set: q })
  },
}