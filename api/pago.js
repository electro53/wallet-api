'use strict'

import { Pago } from '../models'

export default {
  buscarPago: async (q) => {
    return await Pago.findOne({$and:[{id_sesion: q.id}, {token: q.token}]}).populate('usuario')
  },  

  create: (q) => {
    const data = new Pago(q)
    return data.save()
  },

  update: (q) => {
    return Pago.updateOne({ _id: q._id }, { $set: q })
  },
}