'use strict'

import { Recarga } from '../models'

export default {
  findById: (_id) => {
    return Recarga.findById(_id)
  },

  create: (q) => {
    const data = new Recarga(q)
    return data.save()
  }
}