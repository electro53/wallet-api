'use strict'
import mongoose from 'mongoose'
import { mongoUrl } from './config'
import { Usuario, Recarga, Pago } from './routes'

const debug = require('debug')('interfell:server')
const http = require('http')
const chalk = require('chalk')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const port = process.env.PORT || 9000
const app = express()
const server = http.createServer(app)

app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(express.static('public'));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('view engine', 'ejs');
app.use('/api/usuario/', Usuario)
app.use('/api/recarga/', Recarga)
app.use('/api/pago/', Pago)
app.use((err, req, res, next) => {
  debug(`Error: ${err.message}`)

  if (err.message.match('/not found/')) {
    return res.status(404).send({ err: err.message })
  }

  res.status(500).send({ error: err.message })
})

function handleFatalError (err) {
  console.error(`${chalk.red(['fatal error'])} ${err.message}`)
  console.error(err.stack)
}


async function start () {
  const db = await mongoose.connect(mongoUrl)
  if (!module.parent) {
    process.on('uncaughtException', handleFatalError)
    process.on('unhandledRejection', handleFatalError)
    
    server.listen(port, '0.0.0.0', () => {
      console.log(`${chalk.green('[interfell-server]')} server listening on port ${port}`)
    })
  }

}
start()