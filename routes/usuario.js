'use strict'

import express from 'express'
import { Usuario } from '../api'
import { handleError } from '../utils'

  const app = express.Router()
  
  app.get('/:id', async (req, res) => {
    try {
      const data = await Usuario.findById(req.params.id)
      res.status(200).json(data)
    } catch (error) {
      handleError(error, res)
    }
  })
  
  app.post('/', async (req, res) => {
    try {
      let err = []
      Object.keys(req.body).map(key => {
        if (!req.body[key]) {
          err.push(' '+key)
        }
      });
      if (err.length > 0) {
        res.status(201).json({message: 'Estimado usuario, para continuar con el registro debe rellenar los campos: ' + err + '.'})
      } else {
        const data = await Usuario.create(req.body)
        res.status(201).json(data)
      }
    } catch (error) {
      handleError(error, res)
    }
  })

  app.post('/login', async (req, res) => {
    try {
      const usuario = await Usuario.loginUsuario(req.body)
      if(usuario == 'user') {
        res.status(200).json({ status: 'denied', message: 'Usuario no existe'})
      }
      else if(usuario == 'pass'){
        res.status(200).json({ status: 'denied', message: 'Contraseña invalida'})
      }
      else{
        var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
        var codigo = "";
        for (let i = 0; i < 10; i++) {
          codigo +=caracteres.charAt(Math.floor(Math.random()*caracteres.length));
        }
        let info = {
          usuario: usuario,
          id_sesion: codigo
        }
        res.status(201).json(info)
      }
    } catch (error) {
      handleError(error, res)
    }
  })
  
  app.post('/consulta', async (req, res) => {
    try {
      let err = []
      Object.keys(req.body).map(key => {
        if (!req.body[key]) {
          err.push(' '+key)
        }
      });
      if (err.length > 0) {
        res.status(201).json({message: 'Estimado usuario, para continuar con el registro debe rellenar los campos: ' + err + '.'})
      } else {
        const data = await Usuario.consulta(req.body)
        if (data) {
          res.status(201).json(data)
        } else {
          res.status(201).json({message: "Estimado usuario, los datos ingresados no coinciden con ninguna membresía registrada en nuestro sistema, por favor verifique los datos."})
        }
      }
    } catch (error) {
      handleError(error, res)
    }
  })
  
  app.put('/recarga', async (req, res) => {
    try {
      let err = []
      Object.keys(req.body).map(key => {
        if (!req.body[key]) {
          err.push(' '+key)
        }
      });
      if (err.length > 0) {
        res.status(201).json({message: 'Estimado usuario, para continuar con el registro debe rellenar los campos: ' + err + '.'})
      } else {
        const data = await Usuario.update(req.body)
        if (data) {
          res.status(201).json({success: 'Su recarga ha sido realizada exitosamente.'})
        } else {
          res.status(201).json({message: "Estimado usuario, los datos ingresados no coinciden con ninguna membresía registrada en nuestro sistema, por favor verifique los datos."})
        }
      }
    } catch (error) {
      handleError(error, res)
    }
  })
  export default app