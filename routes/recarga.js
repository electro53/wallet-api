'use strict'

import express from 'express'
import { Recarga } from '../api'
import { handleError } from '../utils'

  const app = express.Router()

  app.get('/:id', async (req, res) => {
    try {
      const data = await Recarga.findById(req.params.id)
      res.status(200).json(data)
    } catch (error) {
      handleError(error, res)
    }
  })
  
  app.post('/', async (req, res) => {
    let q = req.body
    try {
      const data = await Recarga.create(q)
      res.status(201).json(data)
    } catch (error) {
      handleError(error, res)
    }
  })
  
  export default app