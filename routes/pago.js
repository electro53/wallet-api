'use strict'

import express from 'express'
import { Pago, Usuario, Email } from '../api'
import { handleError } from '../utils'

  const app = express.Router()

  app.get('/:id', async (req, res) => {
    try {
      const data = await Pago.findById(req.params.id)
      res.status(200).json(data)
    } catch (error) {
      handleError(error, res)
    }
  })
  
  app.post('/', async (req, res) => {
    let q = req.body
    try {
      let err = []
      Object.keys(req.body).map(key => {
        if (!req.body[key]) {
          err.push(' '+key)
        }
      });
      if (err.length > 0) {
        res.status(201).json({message: 'Estimado usuario, para continuar con el registro debe rellenar los campos: ' + err + '.'})
      } else {
        const user = await Usuario.validar(q)
        if (!user) {
          res.status(200).json({message: 'Estimado cliente, el email ingresado no coincide con ninguna membresía registrada en nuestro sistema, por favor verifique los datos.'})
        } else {
          if (q.monto > user.saldo) {
            res.status(200).json({message: 'Estimado cliente, no posee saldo suficiente para cancelar la compra, por favor realice una recarga y vuelva a intentarlo.'})
          } else {
            var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
            var codigo = "";
            for (let i = 0; i < 6; i++) {
              codigo +=caracteres.charAt(Math.floor(Math.random()*caracteres.length));
            }
            q.token= codigo
            q.usuario = user._id
            const data = await Pago.create(q)
            let info = {
              id: data.id_sesion,
              token: q.token
            }
            const not = await Email.enviaremail(q.email, 'Solicitud de pago', 'confirmacion', info)
            res.status(201).json(data)
          }
        }
      }
    } catch (error) {
      handleError(error, res)
    }
  })

  app.put('/', async (req, res) => {
    let q = req.body
    try {
      let err = []
      Object.keys(req.body).map(key => {
        if (!req.body[key]) {
          err.push(' '+key)
        }
      });
      if (err.length > 0) {
        res.status(201).json({message: 'Estimado usuario, para continuar con el registro debe rellenar los campos: ' + err + '.'})
      } else {
        if (req.body.id.length < 10) {
          res.status(201).json({message: 'Estimado usuario, el campo id de sesión debe contener al menos 10 caracteres'})
        } else if (req.body.token.length !== 6) {
          res.status(201).json({message: 'Estimado usuario, el campo token debe contener 6 caracteres'})
        } else {
          const pago = await Pago.buscarPago(q)
          if (pago) {
            if (pago.estatus === "Pagado") {
              res.status(201).json({message: 'Estimado cliente, ya se ha realizado el pago de esta compra.'})
            } else {
              if (pago.monto > pago.usuario.saldo) {
                res.status(201).json({message: 'Estimado cliente, no posee saldo suficiente para cancelar la compra, por favor realice una recarga y vuelva a intentarlo.'})
              } else {
                let info = {
                  _id: pago._id,
                  estatus: "Pagado"
                }
                const data = await Pago.update(info)
                let info2 = {
                  _id: pago.usuario._id,
                  saldo: parseFloat(pago.usuario.saldo) - parseFloat(pago.monto)
                }
                const user = await Usuario.update2(info2)
                res.status(201).json({success: 'Se ha confirmado el pago su compra con éxito.'})
              }
            }
          } else {
            res.status(201).json({message: 'Estimado cliente, los datos ingresados no coinciden con ningún registro en nuestro sistema, por favor verifique la información.'})
          }
        }
      }
    } catch (error) {
      handleError(error, res)
    }
  })
  
  export default app