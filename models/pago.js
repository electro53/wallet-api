'use strict'

import mongoose, { Schema } from 'mongoose'

const { ObjectId } = Schema.Types

const PagoSchema = new Schema({
    usuario           : { type: Schema.Types.ObjectId, ref: 'Usuario' },
    id_sesion         : { type: String, required: true },
    token             : { type: String, required: true },
    monto             : { type: Number, required: true },
    estatus           : { type: String, required: true, default: "Pendiente" }
    })
  
  export default mongoose.model('Pago', PagoSchema)
  