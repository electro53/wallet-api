'use strict'

import mongoose, { Schema } from 'mongoose'

const RecargaSchema = new Schema({
    documento         : { type: String, required: true },
    celular           : { type: String, required: true },
    valor             : { type: Number }
    })
  
  export default mongoose.model('Recarga', RecargaSchema)
  
