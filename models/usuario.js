'use strict'

import mongoose, { Schema } from 'mongoose'

const UsuarioSchema = new Schema({
    nombre            : { type: String, required: true },
    documento         : { type: String, required: true },
    celular           : { type: String, required: true },
    email             : { type: String, required: true },
    password          : { type: String, required: true },
    saldo             : { type: Number, default: 0 }
    })
  
  export default mongoose.model('Usuario', UsuarioSchema)
  
